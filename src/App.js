import logo from './logo.svg';
import './App.css';
import Ex_Shoe_Shop from './ShoeShop/Component/Ex_Shoe_Shop';

function App() {
  return (
    <div>
      <Ex_Shoe_Shop/>
    </div>
  );
}

export default App;
