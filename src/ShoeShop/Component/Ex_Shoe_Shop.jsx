import React, { useState } from 'react'
import { shoeArr } from './dataShoe'
import ListShoe from './ListShoe'
import CartShoe from './CartShoe'
import DetailShoe from './DetailShoe'
export default function Ex_Shoe_Shop() {
    const [arrShoe, setArrShoe] = useState(shoeArr)
    const [arrShoeCart, setArrShoeCart] = useState([])
    const [shoeDetail , setShoeDetail] = useState(arrShoe[1])

   let handleAddToCart = (shoe) => {
     console.log(shoe);
     let cloneCart = [...arrShoeCart]
     let index = cloneCart.findIndex((item)=>{
        return item.id == shoe.id
      })
    if (index == -1){
      let newShoe = shoe
      newShoe.soLuong = 1
      cloneCart.push(newShoe)
    }else {
      cloneCart[index].soLuong += 1
    }
    setArrShoeCart(cloneCart)
    }


    let handleQuantity = (shoe,choice) => {

      let cloneCart = [...arrShoeCart]
      let index = cloneCart.findIndex((item)=>{
        return item.id == shoe.id
      })
      if(index !== -1){
        cloneCart[index].soLuong += choice
      }
      setArrShoeCart(cloneCart)
     cloneCart[index].soLuong == 0 && handleDelete(shoe.id)     
    }

    let handleDelete = (id) => {
      console.log(id);
      let cloneCart = [...arrShoeCart]
     let index = cloneCart.findIndex(item =>{
      return item.id == id
     })
     if(index != -1){
      cloneCart.splice(index,1)
     }
     console.log(cloneCart);
     setArrShoeCart(cloneCart)
    }

    let handleBuy = () => {
      alert('Cám ơn quý khách đã mua hàng')
     setArrShoeCart([])
    }
    let handleViewDetail = (shoe) => {
     setShoeDetail(shoe)
    }

    let tinhTongSoLuong = () => {
     let cloneCart = [...arrShoeCart]
    return cloneCart.reduce((tong, item)=>{
      return tong+= item.soLuong
     },0)
    }

    let tinhTien = () => {
      let cloneCart = [...arrShoeCart]
      let tongTien = cloneCart.reduce((tong, item)=>{
         tong+= item.soLuong*1 * item.price*1
        return tong
       },0)
       return tongTien.toLocaleString()
    }
  return (
    <div>
      <div className='row'>
          <div className='col-11'>
            <ListShoe arrShoe = {arrShoe} handleAddToCart ={handleAddToCart} handleViewDetail={handleViewDetail}/>
          </div>
          <div className='col-1'>
            <CartShoe arrShoeCart ={arrShoeCart} handleQuantity ={handleQuantity} handleDelete ={handleDelete} handleBuy ={handleBuy} tinhTongSoLuong ={tinhTongSoLuong} tinhTien ={tinhTien}/>
          </div>
      </div>
      <DetailShoe shoeDetail ={shoeDetail} handleAddToCart ={handleAddToCart}/>
    </div>
  )
}
