import React from 'react'
import { Card } from 'antd';
const { Meta } = Card;
export default function ItemShoe({shoe, handleAddToCart,handleViewDetail}) {
  return (
    <div className='col-4 d-flex justify-content-center my-2'>
      <Card
    hoverable
    style={{
      width: 340,
    }}
    cover={<img alt="example" src={shoe.image} />}
  >

    <Meta title={shoe.name} description={shoe.description.length > 100 ? shoe.description.slice(0,100)+'...': shoe.description} />
    <h3 className='mt-2 text-center'>$ {shoe.price}</h3>
    <div className='text-center'>
      <button onClick={()=> handleAddToCart(shoe)} className='btn btn-success mt-4 mr-2'>Add to cart</button>
      <button onClick={()=> handleViewDetail(shoe)} data-toggle="modal" data-target=".bd-example-modal-lg" className='btn btn-warning mt-4'>View detail</button>
    </div>
  </Card></div>
  )
}
