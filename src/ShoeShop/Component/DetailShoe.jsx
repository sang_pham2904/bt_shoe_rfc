import React from "react";

export default function DetailShoe({ shoeDetail,handleAddToCart }) {

  let { name, price, description, shortDescription, quantity, image } =
    shoeDetail;

  return (
    <div>

<div>
  <div className="modal fade bd-example-modal-lg" id="detailShoe" tabIndex={-1} role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div className="modal-dialog modal-lg">
      <div className="modal-content">
         
      <div className="text-center">
        <div className="d-flex justify-content-center">
          <img
            className="card-img-top"
            style={{ width: "300px" }}
            src={image}
            alt="Card image cap"
          />
        </div>
        <div className="card-body">
          <h4 className="card-title">{name}</h4>
          <h6 className="card-text">$ {price.toLocaleString()}</h6>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">Current inventory: {quantity}</li>
          <li className="list-group-item">{description}</li>
          <li className="list-group-item">{shortDescription}</li>
        </ul>
      </div>  

      <div className="modal-footer">
              <button className="btn btn-success" onClick={()=> handleAddToCart(shoeDetail)}>Add to cart</button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
      </div>
    </div>
  </div>
</div>



    </div>
  );
}
