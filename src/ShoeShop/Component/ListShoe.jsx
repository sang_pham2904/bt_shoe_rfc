import React from 'react'
import ItemShoe from './ItemShoe'
export default function ListShoe({arrShoe, handleAddToCart,handleViewDetail}) {

  let renderShoe = () => {
   return arrShoe.map((item,index)=>{
    return <ItemShoe key={index} shoe ={item} handleAddToCart ={handleAddToCart} handleViewDetail ={handleViewDetail}/>
   })
  }
  return (
    <div className='row'>
      {renderShoe()}
    </div>
  )
}
