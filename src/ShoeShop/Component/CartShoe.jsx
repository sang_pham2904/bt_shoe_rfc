import React from "react";
import { Space, Table, Tag } from "antd";
export default function CartShoe({
  arrShoeCart,
  handleQuantity,
  handleDelete,
  handleBuy,
  tinhTongSoLuong,
  tinhTien
}) {
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Image",
      dataIndex: "image",
      key: "name",
      render: (image) => <img src={image} alt="" style={{ width: "150px" }} />,
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
      // render: (text) => <a>{text}</a>,
    },
    {
      title: "Quantity",
      dataIndex: "soLuong",
      key: "soLuong",
      render: (soLuong, shoe) => {
        return (
          <div>
            <button
              onClick={() => handleQuantity(shoe, -1)}
              className="btn btn-dark"
            >
              -
            </button>
            <strong className="mx-2">{soLuong}</strong>
            <button
              onClick={() => handleQuantity(shoe, 1)}
              className="btn btn-dark"
            >
              +
            </button>
          </div>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "id",
      key: "id",
      render: (id) => {
        return (
          <div>
            <button onClick={() => handleDelete(id)} className="btn btn-danger">
              Delete
            </button>
          </div>
        );
      },
    },
  ];
  return (
    <div>
      <div>
        <button
          style={{ position: "fixed", top: "20px", right: "1%" }}
          type="button"
          className="btn btn-primary"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            fill="currentColor"
            className="bi bi-cart-check-fill"
            viewBox="0 0 16 16"
          >
            <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-1.646-7.646-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708.708z" />
          </svg>{" "}
          Cart ({tinhTongSoLuong()} )
        </button>
        <div
          className="modal fade  bd-example-modal-xl"
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-xl" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Modal title
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <Table
                  rowKey={(shoe) => {
                    return shoe.id;
                  }}
                  className="text-center"
                  columns={columns}
                  dataSource={arrShoeCart}
                />
              </div>
              <h2 className="text-right mr-5">Total : $ {tinhTien()}</h2>
              <div className="modal-footer">
                
                <button className="btn btn-warning" onClick={handleBuy}>Buy</button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
